<?php
/**
 * Fonction d'upgrade/installation du plugin foundation-4-spip
 *
 * @plugin	   foundation_6
 * @copyright  2013
 * @author	   Phenix
 * @licence	   GNU/GPL
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Inclure les fonctions de foundation
include_spip('inc/foundation');

/**
 * Rendre les iframes responsives via un filtre et la classe responsive-embed de
 * Foundation.
 *
 * @param string $texte HTML dans lequel chercher des iframes
 * @access public
 * @return string
 */
function filtre_responsive_embed_dist($texte) {
	include_spip('inc/foundation');
	// On détecte toute les iFrames et on les rends responsives.
	return preg_replace_callback('%<iframe(.*)></iframe>%m', 'responsive', $texte);
}

/**
 * Assurer la rétro-compatibilité avec l'ancien nom de ce filtre
 *
 * @deprecated Utiliser filtre_responsive_embed_dist()
 * @see filtre_responsive_embed_dist()
 *
 * @param mixed $texte
 * @access public
 * @return mixed
 */
function filtre_iframe_responsive($texte) {
	$responsive_embed = charger_filtre('responsive_embed');
	return $responsive_embed($texte);
}

/**
 * Cette balise va permettre de rendre le squelette compatible avec toutes les
 * versions de Foundation.
 *
 * La syntaxe est la suivante:
 *
 * ```
 * [(#COLONNES{#ARRAY{large, 4, medium, 3, small, 3}})]
 * ```
 *
 * Pour activer le calcule automatique en fonction du total d'une boucle :
 *
 * ```
 * [(#COLONNES{#ARRAY{large, 4*, medium, 3*, small, 3*}})]
 * ```
 *
 * @param mixed $p
 * @access public
 * @return mixed
 */
function balise_COLONNES_dist($p) {
	// On récupère les paramètres de la balise.
	$nombre_colonnes = interprete_argument_balise(1, $p);
	$type = interprete_argument_balise(2, $p);

	// Dans le cas ou on ce trouve dans une boucle SPIP, on va passer le total
	// de la boucle à la fonction class_grid_foundation.
	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if ($b !== '') {
		$total_boucle = "\$Numrows['$b']['total']";
		$p->boucles[$b]->numrows = true;
	} else {
		$total_boucle = 'null';
	}

	// On met une valeur par défaut à type.
	if (!$type) {
		$type = "'large'";
	}

	// On calcule la class
	$p->code = "class_grid_foundation($nombre_colonnes, $type, $total_boucle).'columns'";
	$p->interdire_scripts = false;

	return $p;
}

/**
 * Cette balise va permettre de rendre le squelette compatible avec toutes les
 * versions de Foundation.
 *
 * La syntaxe est la suivante:
 *
 * ```
 * [(#BLOCKGRID{#ARRAY{large-up, 4, medium-up, 3, small-up, 3}})]
 * ```
 *
 * Pour activer le calcule automatique en fonction du total d'une boucle :
 *
 * ```
 * [(#BLOCKGRID{#ARRAY{large-up, 4*, medium-up, 3*, small-up, 3*}})]
 * ```
 *
 * @param mixed $p
 * @access public
 * @return mixed
 */
function balise_BLOCKGRID_dist($p) {
	// On récupère les paramètres de la balise.
	$nombre_colonnes = interprete_argument_balise(1, $p);
	$type = interprete_argument_balise(2, $p);

	// Dans le cas ou on ce trouve dans une boucle SPIP, on va passer le total
	// de la boucle à la fonction class_grid_foundation.
	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if ($b !== '') {
		$total_boucle = "\$Numrows['$b']['total']";
		$p->boucles[$b]->numrows = true;
	} else {
		$total_boucle = 'null';
	}

	// On met une valeur par défaut à type.
	if (!$type) {
		$type = "'large-up'";
	}

	// On calcule la class
	$p->code = "class_blocs_foundation($nombre_colonnes, $type, $total_boucle).'row'";
	$p->interdire_scripts = false;

	return $p;
}

/**
 * Filtre pour afficher des étoiles à la suite via les icônes Foundation.
 *
 * @param int $nombre
 * @access public
 * @return string
 */
function filtre_etoile_foundation_dist($nombre) {

	$config = lire_config('foundation');

	if (!$config['foundation-icons']) {
		return '<span>Les icones foundation ne sont pas activée !</span>';
	}

	$etoile = '<span class="foundation_etoile">';
	for ($i=0; $i<$nombre; $i++) {
		$etoile .= '<span class="fi-star"></span>';
	}
	$etoile .= '</span>';

	return $etoile;
}

if (!function_exists('balise_LIRE_CONSTANTE_dist')) {
	/**
	 * Balise LIRE_CONSTANT pour SPIP
	 *
	 * @param mixed $p
	 * @access public
	 * @return mixed
	 */
	function balise_LIRE_CONSTANTE_dist($p) {
		$constante = interprete_argument_balise(1, $p);

		$p->code = "constant($constante)";

		$p->interdire_scripts = false;

		return $p;
	}
}

function balise_CALCULER_COLONNES_dist($p) {
	// Nombre maximum de colonne
	$max = interprete_argument_balise(1, $p);

	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if ($b === '' || !isset($p->boucles[$b])) {
		$msg = array(
			'zbug_champ_hors_boucle',
			array('champ' => "#$b" . 'CALCULER_COLONNES')
		);
		erreur_squelette($msg, $p);
	} else {
		$p->code = "calculer_colonnes($max, \$Numrows['$b']['total'])";
		$p->boucles[$b]->numrows = true;
		$p->interdire_scripts = false;
	}

	return $p;
}
